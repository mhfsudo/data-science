import twitter
import sys

def oauth_login():

    CONSUMER_KEY = '<key>'
    CONSUMER_SECRET = '<key>'
    OAUTH_TOKEN = '<key>'
    OAUTH_TOKEN_SECRET = '<key>'

    auth = twitter.oauth.OAuth(OAUTH_TOKEN, OAUTH_TOKEN_SECRET, CONSUMER_KEY, CONSUMER_SECRET)

    twitter_api = twitter.Twitter(auth=auth)
    return twitter_api

    twitter_api = oauth_login()

    print(twitter_api)

# Finding topics of interest by using the filtering capabilities it offers.
# Query terms

    q = 'Open Source' # Comma-separated list of terms

    print('Filtering the public timeline for track={0}'.format(q), file=sys.stderr)
    sys.stderr.flush()

# Returns an instance of twitter.Twitter
    twitter_api = oauth_login()

# Reference the self.auth parameter
    twitter_stream = twitter.TwitterStream(auth=twitter_api.auth)

# See https://developer.twitter.com/en/docs/tutorials/consuming-streaming-data
    stream = twitter_stream.statuses.filter(track=q)

for tweet in stream:
    print(tweet['text'])
    sys.stdout.flush()

# Save to a database in a particular collection